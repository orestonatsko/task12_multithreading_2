package com.epamcourses.orestonatsko.menu;

import com.epamcourses.orestonatsko.tasks.MedicalClinic;
import com.epamcourses.orestonatsko.tasks.task1.LockGrail;
import com.epamcourses.orestonatsko.tasks.task2.BlockingQueueExample;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Menu {
    private Map<String, String> menu;
    private Map<String, Command> methods;
    private Scanner input;

    public Menu() {
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        methods = new HashMap<>();
        initMenu();
    }

    private void initMenu() {
        menu.put("1", "Run Medical Clinic");
        menu.put("2", "Run Lock Example");
        menu.put("3", "Run BlockQueue Example");

        methods.put("1", new MedicalClinic());
        methods.put("2", new LockGrail());
        methods.put("3", new BlockingQueueExample());
    }

    public void show() {
        String userInput;
        do {
            System.out.println("\n\t\t~~MENU~~");
            menu.forEach((k, v) -> System.out.println(k + " - " + v));
            System.out.println("Q - Quit");
            userInput = input.next();
            try {
                methods.get(userInput).execute();
            } catch (NullPointerException e) {/*ignore*/}
        } while (!userInput.equalsIgnoreCase("Q"));
    }
}