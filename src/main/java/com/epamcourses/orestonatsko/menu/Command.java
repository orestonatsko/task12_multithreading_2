package com.epamcourses.orestonatsko.menu;

public interface Command {
    void execute();
}
